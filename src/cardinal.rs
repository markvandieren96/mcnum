#[derive(Debug, Copy, Clone)]
pub enum Cardinal {
	North,
	East,
	South,
	West,
	Top,
	Bottom,
}

impl Cardinal {
	pub fn from_axis_index_and_backface(axis_index: usize, backface: bool) -> Option<Cardinal> {
		match axis_index {
			0 => {
				if backface {
					Some(Cardinal::East)
				} else {
					Some(Cardinal::West)
				}
			}
			1 => {
				if backface {
					Some(Cardinal::Bottom)
				} else {
					Some(Cardinal::Top)
				}
			}
			2 => {
				if backface {
					Some(Cardinal::South)
				} else {
					Some(Cardinal::North)
				}
			}
			_ => None,
		}
	}
}
