use crate::Vec2;
use serde::{Deserialize, Serialize};
use std::cmp::PartialOrd;
use std::ops::{Add, Mul, Sub};

pub type Recti = Rect<i32>;

#[derive(Default, Copy, Clone, Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct Rect<T> {
	pub x: T,
	pub y: T,
	pub w: T,
	pub h: T,
}

impl<T> Rect<T> {
	pub fn new(x: T, y: T, w: T, h: T) -> Rect<T> {
		Rect { x, y, w, h }
	}
	pub fn new_pos_size(pos: Vec2<T>, size: Vec2<T>) -> Rect<T> {
		Rect::new(pos.x, pos.y, size.x, size.y)
	}

	pub fn new_min_max(min: Vec2<T>, max: Vec2<T>) -> Rect<T>
	where
		Vec2<T>: Sub<Output = Vec2<T>>,
		T: Copy,
	{
		Rect::new_pos_size(min, max - min)
	}

	pub fn pos(self) -> Vec2<T> {
		Vec2::new(self.x, self.y)
	}

	pub fn size(self) -> Vec2<T> {
		Vec2::new(self.w, self.h)
	}

	pub fn with_pos(self, pos: Vec2<T>) -> Self {
		Rect::new_pos_size(pos, self.size())
	}

	pub fn with_size(self, size: Vec2<T>) -> Self {
		Rect::new_pos_size(self.pos(), size)
	}

	pub fn translate(self, translation: Vec2<T>) -> Self
	where
		Vec2<T>: Add<Output = Vec2<T>>,
		T: Copy,
	{
		Rect::new_pos_size(self.pos() + translation, self.size())
	}

	pub fn scale(self, scale: T) -> Self
	where
		Vec2<T>: Mul<Output = Vec2<T>>,
		T: Copy,
	{
		Rect::new_pos_size(self.pos(), self.size() * Vec2::new_xy(scale))
	}

	pub fn scale2(self, scale: Vec2<T>) -> Self
	where
		Vec2<T>: Mul<Output = Vec2<T>>,
		T: Copy,
	{
		Rect::new_pos_size(self.pos(), self.size() * scale)
	}

	pub fn contains_point(self, point: Vec2<T>) -> bool
	where
		T: PartialOrd + Add<Output = T> + Copy,
	{
		point.x >= self.min().x && point.y >= self.min().y && point.x < self.max().x && point.y < self.max().y
	}

	pub fn min(self) -> Vec2<T> {
		Vec2::new(self.x, self.y)
	}

	pub fn max(self) -> Vec2<T>
	where
		T: Add<Output = T>,
	{
		Vec2::new(self.x + self.w, self.y + self.h)
	}

	pub fn fixit(&mut self)
	where
		T: PartialOrd + Add + num::Zero + num::Signed + Copy,
	{
		if self.w < num::zero() {
			self.x = self.x + self.w;
			self.w = num::abs(self.w);
		}
		if self.y < num::zero() {
			self.y = self.y + self.h;
			self.h = num::abs(self.h);
		}
	}
}

impl Recti {
	pub fn new_unit() -> Recti {
		Rect::new_pos_size(Vec2::zero(), Vec2::new_xy(1))
	}
}
