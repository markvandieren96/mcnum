struct DataPoint<T> {
	pub a: f32,
	pub val: T,
}

impl<T> DataPoint<T> {
	pub fn new(a: f32, val: T) -> DataPoint<T> {
		DataPoint { a, val }
	}
}

pub struct Tcurve<T> {
	points: Vec<DataPoint<T>>,
}

impl<T> Default for Tcurve<T> {
	fn default() -> Tcurve<T> {
		Tcurve { points: Vec::new() }
	}
}

impl<T> Tcurve<T> {
	pub fn set(&mut self, a: f32, val: T) {
		for i in 0..self.points.len() {
			if a < self.points[i].a {
				self.points.insert(i, DataPoint::new(a, val));
				return;
			}
		}
		self.points.push(DataPoint::new(a, val));
	}

	pub fn set_range(&mut self, a: f32, b: f32, val: T)
	where
		T: Clone,
	{
		self.set(a, val.clone());
		self.set(b, val);
	}

	pub fn get(&self, a: f32) -> Result<T, String>
	where
		T: std::ops::Add<Output = T> + std::ops::Sub<Output = T> + std::ops::Sub<Output = T> + std::ops::Mul<f32, Output = T> + Clone,
	{
		if self.points.is_empty() {
			Err("Could not interpolate point on curve, because curve has 0 points!".to_owned())
		} else if self.points.len() == 1 || a <= self.points[0].a {
			Ok(self.points[0].val.clone())
		} else if a >= self.points[self.points.len() - 1].a {
			Ok(self.points[self.points.len() - 1].val.clone())
		} else {
			let mut right_index = 0;
			for i in 0..self.points.len() {
				if self.points[i].a > a {
					right_index = i;
					break;
				}
			}

			let left_point = &self.points[right_index - 1];
			let right_point = &self.points[right_index];
			let range = right_point.a - left_point.a;
			let alpha = normalize(a, left_point.a, range);
			Ok(crate::lerp(&left_point.val, &right_point.val, alpha))
		}
	}
}

fn normalize(f: f32, min: f32, range: f32) -> f32 {
	(f - min) / range
}
