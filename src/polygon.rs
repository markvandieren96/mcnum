use super::Vec2f;

pub struct Polygon {
	pub vertices: Vec<Vec2f>,
}

impl Polygon {
	pub fn new(vertices: Vec<Vec2f>) -> Polygon {
		Polygon { vertices }
	}

	pub fn new_empty() -> Polygon {
		Polygon { vertices: Vec::new() }
	}

	pub fn centroid(&self) -> Vec2f {
		find_centroid(&self.vertices)
	}
}

// https://stackoverflow.com/questions/2792443/finding-the-centroid-of-a-polygon
pub fn find_centroid(vertices: &[Vec2f]) -> Vec2f {
	if vertices.is_empty() {
		panic!("Can't calculate centroid of a polygon with 0 vertices!");
	}

	let mut centroid = Vec2f::default();
	let mut signed_area = 0.0;

	for i in 0..vertices.len() - 1 {
		let v0 = vertices[i];
		let v1 = vertices[i + 1];
		let partial_signed_area = v0.x * v1.y - v1.x * v0.y;
		signed_area += partial_signed_area;
		centroid += (v0 + v1) * partial_signed_area;
	}

	let v0 = vertices[vertices.len() - 1];
	let v1 = vertices[0];
	let partial_signed_area = v0.x * v1.y - v1.x * v0.y;
	signed_area += partial_signed_area;
	centroid += (v0 + v1) * partial_signed_area;

	signed_area *= 0.5;
	centroid.x /= 6.0 * signed_area;
	centroid.y /= 6.0 * signed_area;

	centroid
}
