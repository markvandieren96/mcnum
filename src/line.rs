use super::Vec2f;

#[derive(Copy, Clone, Debug, Default)]
pub struct Line {
	pub origin: Vec2f,
	pub dir: Vec2f,
}

impl Line {
	pub fn new(origin: Vec2f, dir: Vec2f) -> Line {
		Line { origin, dir }
	}

	pub fn new_ab(a: Vec2f, b: Vec2f) -> Line {
		Line { origin: a, dir: b - a }
	}

	pub fn midpoint(&self) -> Vec2f {
		self.at(0.5)
	}

	pub fn perp_dir(&self) -> Vec2f {
		Vec2f::new(-self.dir.y, self.dir.x)
	}

	// A perpendicular line, starting from the middle of this line
	pub fn bisector(&self) -> Line {
		let midpoint = self.midpoint();
		Line::new_ab(midpoint, midpoint + self.perp_dir())
	}

	pub fn at(&self, t: f32) -> Vec2f {
		self.origin + self.dir * t
	}

	// https://stackoverflow.com/questions/28157088/finding-the-intersection-of-2-lines-defined-by-2-points
	pub fn intersect(line1: Line, line2: Line) -> Option<Vec2f> {
		let d = line1.dir.x * line2.dir.y - line1.dir.y * line2.dir.x;
		if d.abs() < 0.0001 {
			None
		} else {
			let n = (line2.origin.x - line1.origin.x) * line2.dir.y - (line2.origin.y - line1.origin.y) * line2.dir.x;
			Some(line1.at(n / d))
		}
	}
}

/*
inline double intersection(const Line2D& a, const Line2D& b) {
	const double Precision = std::sqrt(std::numeric_limits<double>::epsilon());
	double d = a.v().x() * b.v().y() - a.v().y() * b.v().x();
	if(std::abs(d) < Precision) return std::numeric_limits<double>::quiet_NaN();
	else {
		double n = (b.p().x() - a.p().x()) * b.v().y()
				 - (b.p().y() - a.p().y()) * b.v().x();
		return n/d;
	}
}
*/
