use super::*;
use serde::{Deserialize, Serialize};
use std::ops::Mul;

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq)]
pub struct Quat(pub glam::Quat);

impl Quat {
	pub fn new(angle: f32, axis: Vec3f) -> Self {
		Quat(glam::Quat::from_axis_angle(axis.normalized().into(), angle))
	}

	pub fn new_raw(components: [f32; 4]) -> Self {
		Quat(glam::Quat::from_slice(&components))
	}

	pub fn new_euler(angles: Vec3f) -> Self {
		Quat(glam::Quat::from_euler(glam::EulerRot::XYZ, angles.x, angles.y, angles.z))
	}

	pub fn identity() -> Self {
		Quat(glam::Quat::IDENTITY)
	}

	pub fn slerp(q1: Quat, q2: Quat, alpha: f32) -> Self {
		Quat(q1.0.slerp(q2.0, alpha))
	}
}

impl Default for Quat {
	fn default() -> Self {
		Self::identity()
	}
}

impl From<glam::Quat> for Quat {
	fn from(q: glam::Quat) -> Self {
		Quat(q)
	}
}

impl Mul<Vec3f> for Quat {
	type Output = Vec3f;
	fn mul(self, v: Vec3f) -> Vec3f {
		self.0.mul_vec3(v.to_glam()).into()
	}
}

impl Mul<Quat> for Quat {
	type Output = Quat;
	fn mul(self, other: Quat) -> Self {
		Quat::from(self.0 * other.0)
	}
}

impl std::fmt::Display for Quat {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "({}, {}, {}, {})", self.0.x, self.0.y, self.0.z, self.0.w)
	}
}
