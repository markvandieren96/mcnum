pub trait Sqrt {
	fn sqrt(&self) -> Self;
}

#[macro_export]
macro_rules! impl_sqrt {
	(integer $ty:ty) => {
		impl Sqrt for $ty {
			fn sqrt(&self) -> Self {
				num::integer::Roots::sqrt(self)
			}
		}
	};
	(float $ty:ty) => {
		impl Sqrt for $ty {
			fn sqrt(&self) -> Self {
				num::Float::sqrt(*self)
			}
		}
	};
}

impl_sqrt!(integer u8);
impl_sqrt!(integer u16);
impl_sqrt!(integer u32);
impl_sqrt!(integer u64);

impl_sqrt!(integer i8);
impl_sqrt!(integer i16);
impl_sqrt!(integer i32);
impl_sqrt!(integer i64);

impl_sqrt!(float f32);
impl_sqrt!(float f64);
