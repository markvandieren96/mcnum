pub mod cardinal;
pub mod curve;
pub mod line;
pub mod mat4;
pub mod polygon;
pub mod quat;
pub mod rect;
pub mod rectf;
pub mod side;
pub mod sqrt;
pub mod tcurve;
pub mod triangle;
pub mod vec2;
pub mod vec3;
pub mod vec3f;
pub mod vec4;

pub use cardinal::*;
pub use curve::*;
pub use line::*;
pub use mat4::*;
pub use polygon::*;
pub use quat::*;
pub use rect::*;
pub use rectf::*;
pub use side::*;
pub use tcurve::*;
pub use triangle::*;
pub use vec2::*;
pub use vec3::*;
pub use vec3f::*;
pub use vec4::*;

pub type GlamQuat = glam::f32::Quat;
pub use glam;

use std::ops::{Add, Div, Mul, Sub};

pub fn approx_eq(a: f32, b: f32, epsilon: f32) -> bool {
	a > b - epsilon && a < b + epsilon
}

pub fn lerp<T>(start: &T, end: &T, percent: f32) -> T
where
	T: Add<Output = T> + Sub<Output = T> + Mul<f32, Output = T> + Clone,
{
	start.clone() + (end.clone() - start.clone()) * percent
}

pub fn round_to_nearest_multiple<T>(t: T, multiple: T, half_multiple: T) -> T
where
	T: Add<Output = T> + Div<Output = T> + Mul<Output = T> + Copy,
{
	((t + half_multiple) / multiple) * multiple
}

pub fn normalize(f: f32, min: f32, max: f32) -> f32 {
	(f - min) / (max - min)
}
