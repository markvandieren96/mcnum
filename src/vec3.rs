use super::{Vec2, Vec3f};
use serde::{Deserialize, Serialize};
use std::convert::TryFrom;
use std::{fmt, fmt::Display, ops::*};

pub type Vec3i = Vec3<i32>;
pub type Vec3u = Vec3<u32>;

pub fn vec3<T>(x: T, y: T, z: T) -> Vec3<T> {
	Vec3::new(x, y, z)
}

pub fn left<T>() -> Vec3<T>
where
	T: Neg<Output = T> + Copy + num::Zero + num::One,
{
	Vec3::new(-num::one::<T>(), num::zero(), num::zero())
}

pub fn right<T>() -> Vec3<T>
where
	T: Copy + num::Zero + num::One,
{
	Vec3::new(num::one(), num::zero(), num::zero())
}

pub fn bottom<T>() -> Vec3<T>
where
	T: Neg<Output = T> + Copy + num::Zero + num::One,
{
	Vec3::new(num::zero(), -num::one::<T>(), num::zero())
}

pub fn top<T>() -> Vec3<T>
where
	T: Copy + num::Zero + num::One,
{
	Vec3::new(num::zero(), num::one(), num::zero())
}

pub fn back<T>() -> Vec3<T>
where
	T: Neg<Output = T> + Copy + num::Zero + num::One,
{
	Vec3::new(num::zero(), num::zero(), -num::one::<T>())
}

pub fn front<T>() -> Vec3<T>
where
	T: Copy + num::Zero + num::One,
{
	Vec3::new(num::zero(), num::zero(), num::one())
}

#[repr(C)]
#[derive(Serialize, Deserialize, Copy, Clone, Eq, PartialEq, PartialOrd, Ord, Debug, Hash, Default)]
pub struct Vec3<T> {
	pub x: T,
	pub y: T,
	pub z: T,
}

impl<T> From<[T; 3]> for Vec3<T>
where
	T: Copy,
{
	fn from(v: [T; 3]) -> Self {
		Vec3 { x: v[0], y: v[1], z: v[2] }
	}
}

impl<T> Vec3<T> {
	pub fn zero() -> Vec3<T>
	where
		T: Default,
	{
		Vec3 {
			x: Default::default(),
			y: Default::default(),
			z: Default::default(),
		}
	}

	pub fn new(x: T, y: T, z: T) -> Vec3<T> {
		Vec3 { x, y, z }
	}

	pub fn new_xyz(xyz: T) -> Vec3<T>
	where
		T: Copy,
	{
		Vec3 { x: xyz, y: xyz, z: xyz }
	}

	pub fn indexify(&self, dimension: T) -> T
	where
		T: Mul<Output = T> + Add<Output = T> + Copy,
	{
		self.x + self.y * dimension + self.z * dimension * dimension
	}

	pub fn indexify3(&self, dimensions: Vec3<T>) -> T
	where
		T: Mul<Output = T> + Add<Output = T> + Copy,
	{
		self.x + self.y * dimensions.x + self.z * dimensions.x * dimensions.y
	}

	pub fn len_sqr(&self) -> T
	where
		T: Mul<Output = T> + Add<Output = T> + Copy,
	{
		self.x * self.x + self.y * self.y + self.z * self.z
	}

	pub fn dot(&self, other: Self) -> T
	where
		T: Mul<Output = T> + Add<Output = T> + Copy,
	{
		self.x * other.x + self.y * other.y + self.z * other.z
	}

	pub fn cross(&self, other: Self) -> Self
	where
		T: Mul<Output = T> + Sub<Output = T> + Copy,
	{
		Self::new(self.y * other.z - self.z * other.y, self.z * other.x - self.x * other.z, self.x * other.y - self.y * other.x)
	}

	pub fn xz(&self) -> Vec2<T>
	where
		T: Copy,
	{
		Vec2::new(self.x, self.z)
	}

	pub fn xy(&self) -> Vec2<T>
	where
		T: Copy,
	{
		Vec2::new(self.x, self.y)
	}
}

impl<T> Vec3<T>
where
	T: num::Float,
{
	pub fn pitch(&self) -> T {
		self.y.asin()
	}

	pub fn yaw(&self) -> T {
		self.x.atan2(self.z)
	}

	pub fn from_pitch_yaw(pitch: T, yaw: T) -> Vec3<T> {
		Vec3::new(yaw.sin() * pitch.cos(), pitch.sin(), yaw.cos() * pitch.cos())
	}
}

impl<T> Vec3<T>
where
	T: crate::sqrt::Sqrt + num::Zero,
{
	pub fn length(&self) -> T
	where
		T: Mul<Output = T> + Add<Output = T> + Copy,
	{
		self.len_sqr().sqrt()
	}

	pub fn normalized(&self) -> Option<Vec3<T>>
	where
		T: Mul<Output = T> + Div<Output = T> + Add<Output = T> + Copy,
	{
		let len = self.len_sqr().sqrt();
		if len.is_zero() {
			None
		} else {
			Some(*self / len)
		}
	}
}

impl Vec3<f64> {
	pub fn to_f32(self) -> Vec3f {
		Vec3f::new(self.x as f32, self.y as f32, self.z as f32)
	}
}

impl<T> Vec3<T>
where
	T: Copy + num::Zero + num::One + Sub<Output = T>,
{
	pub fn left(&self) -> Vec3<T> {
		Vec3::new(self.x - num::one(), self.y, self.z)
	}

	pub fn right(&self) -> Vec3<T> {
		Vec3::new(self.x + num::one(), self.y, self.z)
	}

	pub fn bottom(&self) -> Vec3<T> {
		Vec3::new(self.x, self.y - num::one(), self.z)
	}

	pub fn top(&self) -> Vec3<T> {
		Vec3::new(self.x, self.y + num::one(), self.z)
	}

	pub fn back(&self) -> Vec3<T> {
		Vec3::new(self.x, self.y, self.z - num::one())
	}

	pub fn front(&self) -> Vec3<T> {
		Vec3::new(self.x, self.y, self.z + num::one())
	}
}

impl<T> Vec3<T>
where
	T: num::ToPrimitive,
{
	pub fn as_index(&self) -> (usize, usize, usize) {
		(self.x.to_usize().unwrap(), self.y.to_usize().unwrap(), self.z.to_usize().unwrap())
	}

	pub fn to_vec3f(&self) -> Vec3f {
		Vec3f::new(self.x.to_f32().unwrap(), self.y.to_f32().unwrap(), self.z.to_f32().unwrap())
	}
}

impl<T> Vec3<T>
where
	T: Copy + num::Signed,
{
	pub fn abs(&self) -> Vec3<T> {
		Vec3::new(self.x.abs(), self.y.abs(), self.z.abs())
	}
}

impl From<Vec3u> for Vec3i {
	fn from(v: Vec3u) -> Self {
		Vec3::new(v.x as i32, v.y as i32, v.z as i32)
	}
}

impl From<Vec3i> for Vec3u {
	fn from(v: Vec3i) -> Self {
		Vec3::new(v.x as u32, v.y as u32, v.z as u32)
	}
}

// Expects the format "(x, y, z)"
impl<T> TryFrom<&str> for Vec3<T>
where
	T: std::str::FromStr,
	<T as std::str::FromStr>::Err: std::fmt::Debug,
{
	type Error = ();

	fn try_from(s: &str) -> Result<Self, ()> {
		let mut iter = s[1..s.len() - 1].split(',').map(|s| s.trim()).map(|s| s.parse::<T>().ok());
		Ok(Vec3::new(iter.next().flatten().ok_or(())?, iter.next().flatten().ok_or(())?, iter.next().flatten().ok_or(())?))
	}
}

impl<T> Vec3<T>
where
	T: Copy,
{
	pub fn to_vec3i(self) -> Vec3i
	where
		i32: std::convert::From<T>,
	{
		Vec3::new(i32::from(self.x), i32::from(self.y), i32::from(self.z))
	}
}

impl Add<Vec3i> for Vec3u {
	type Output = Vec3u;

	fn add(self, other: Vec3i) -> Vec3u {
		(Vec3i::from(self) + other).into()
	}
}

impl<T> Add<Vec3<T>> for Vec3<T>
where
	T: Add<Output = T> + Copy,
{
	type Output = Vec3<T>;

	fn add(self, other: Vec3<T>) -> Vec3<T> {
		Vec3::new(self.x + other.x, self.y + other.y, self.z + other.z)
	}
}

impl<T> AddAssign<Vec3<T>> for Vec3<T>
where
	T: Add<Output = T> + Copy,
{
	fn add_assign(&mut self, other: Vec3<T>) {
		*self = *self + other;
	}
}

impl<T> Sub<Vec3<T>> for Vec3<T>
where
	T: Sub<Output = T> + Copy,
{
	type Output = Vec3<T>;

	fn sub(self, other: Vec3<T>) -> Vec3<T> {
		Vec3::new(self.x - other.x, self.y - other.y, self.z - other.z)
	}
}

impl<T> SubAssign<Vec3<T>> for Vec3<T>
where
	T: Sub<Output = T> + Copy,
{
	fn sub_assign(&mut self, other: Vec3<T>) {
		*self = *self - other;
	}
}

impl<T> Rem<T> for Vec3<T>
where
	T: Rem<Output = T> + Copy,
{
	type Output = Vec3<T>;

	fn rem(self, val: T) -> Vec3<T> {
		Vec3::new(self.x % val, self.y % val, self.z % val)
	}
}

impl<T> Mul<T> for Vec3<T>
where
	T: Mul<Output = T> + Copy,
{
	type Output = Vec3<T>;

	fn mul(self, val: T) -> Vec3<T> {
		Vec3::new(self.x * val, self.y * val, self.z * val)
	}
}

impl<T> Mul<Vec3<T>> for Vec3<T>
where
	T: Mul<Output = T> + Copy,
{
	type Output = Vec3<T>;

	fn mul(self, val: Vec3<T>) -> Vec3<T> {
		Vec3::new(self.x * val.x, self.y * val.y, self.z * val.z)
	}
}

impl<T> MulAssign<Vec3<T>> for Vec3<T>
where
	T: Mul<Output = T> + Copy,
{
	fn mul_assign(&mut self, other: Vec3<T>) {
		*self = *self * other;
	}
}

impl<T> Div<T> for Vec3<T>
where
	T: Div<Output = T> + Copy,
{
	type Output = Vec3<T>;

	fn div(self, val: T) -> Vec3<T> {
		Vec3::new(self.x / val, self.y / val, self.z / val)
	}
}

impl<T> Div<Vec3<T>> for Vec3<T>
where
	T: Div<Output = T> + Copy,
{
	type Output = Vec3<T>;

	fn div(self, other: Vec3<T>) -> Vec3<T> {
		Vec3::new(self.x / other.x, self.y / other.y, self.z / other.z)
	}
}

impl<T> Neg for Vec3<T>
where
	T: Neg<Output = T> + Copy,
{
	type Output = Vec3<T>;

	fn neg(self) -> Vec3<T> {
		Vec3::new(-self.x, -self.y, -self.z)
	}
}

impl<T> Display for Vec3<T>
where
	T: Display,
{
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "({}, {}, {})", self.x, self.y, self.z)
	}
}

impl<T> Index<usize> for Vec3<T>
where
	T: Default + Clone,
{
	type Output = T;

	fn index(&self, index: usize) -> &T {
		match index {
			0 => &self.x,
			1 => &self.y,
			2 => &self.z,
			_ => {
				println!("Index is not in range! index={}, size=3", index);
				panic!("Index is not in range!")
			}
		}
	}
}

impl<T> IndexMut<usize> for Vec3<T>
where
	T: Default + Clone,
{
	fn index_mut(&mut self, index: usize) -> &mut T {
		match index {
			0 => &mut self.x,
			1 => &mut self.y,
			2 => &mut self.z,
			_ => {
				println!("Index is not in range! index={}, size=3", index);
				panic!("Index is not in range!")
			}
		}
	}
}

#[cfg(test)]
mod test {
	use super::super::*;
	use super::Vec3;

	const BIG_EPSILON: f32 = 0.0001;

	#[test]
	fn vec_equality() {
		assert_eq!(Vec3::new(4, 3, 2), Vec3::new(4, 3, 2));
		assert_ne!(Vec3::new(4, 3, 2), Vec3::new(4, 3, 0));

		assert_eq!(Vec3::new(4, 3, 2), Vec3::new(4, 3, 2));
		assert_ne!(Vec3::new(4, 3, 2), Vec3::new(2, 3, 4));
	}

	#[test]
	fn vec_new() {
		assert_eq!(Vec3::new(4, 3, 2), Vec3 { x: 4, y: 3, z: 2 });
	}

	#[test]
	fn vec_new_xyz() {
		assert_eq!(Vec3::new_xyz(4), Vec3 { x: 4, y: 4, z: 4 });
	}

	#[test]
	fn vec_zero() {
		assert_eq!(Vec3::new(0, 0, 0), Vec3::zero());
		assert_eq!(Vec3 { x: 0, y: 0, z: 0 }, Vec3::zero());
	}

	#[test]
	fn vec_ops() {
		assert_eq!(Vec3::new(4, 3, 2) + Vec3::new(4, 3, 2), Vec3::new(8, 6, 4));
		assert_eq!(Vec3::new(4, 3, 2) - Vec3::new(4, 3, 2), Vec3::new(0, 0, 0));

		assert_eq!(Vec3::new(4, 3, 2) * 2, Vec3::new(8, 6, 4));
		assert_eq!(Vec3::new(8, 6, 4) / 2, Vec3::new(4, 3, 2));

		assert_eq!(Vec3::new(4, 3, 2) % 2, Vec3::new(0, 1, 0));
		assert_eq!(Vec3::new(8, 6, 4) % 2, Vec3::new(0, 0, 0));
		assert_eq!(Vec3::new(9, 4, 7) % 3, Vec3::new(0, 1, 1));
	}

	#[test]
	fn vec_indexify() {
		assert_eq!(Vec3::new(0, 0, 0).indexify(0), 0);
		assert_eq!(Vec3::new(0, 0, 0).indexify(4), 0);

		assert_eq!(Vec3::new(1, 0, 0).indexify(4), 1);
		assert_eq!(Vec3::new(2, 0, 0).indexify(4), 2);
		assert_eq!(Vec3::new(3, 0, 0).indexify(4), 3);
		assert_eq!(Vec3::new(4, 0, 0).indexify(4), 4);

		assert_eq!(Vec3::new(0, 1, 0).indexify(4), 4);
		assert_eq!(Vec3::new(1, 1, 0).indexify(4), 5);
		assert_eq!(Vec3::new(2, 1, 0).indexify(4), 6);
		assert_eq!(Vec3::new(3, 1, 0).indexify(4), 7);
		assert_eq!(Vec3::new(0, 2, 0).indexify(4), 8);
		assert_eq!(Vec3::new(0, 3, 0).indexify(4), 12);
		assert_eq!(Vec3::new(0, 4, 0).indexify(4), 16);

		assert_eq!(Vec3::new(0, 0, 1).indexify(4), 16);
		assert_eq!(Vec3::new(0, 0, 2).indexify(4), 32);
		assert_eq!(Vec3::new(0, 0, 3).indexify(4), 48);
		assert_eq!(Vec3::new(0, 0, 4).indexify(4), 64);
	}

	#[test]
	fn vec_indexify3() {
		assert_eq!(Vec3::new(0, 0, 0).indexify(0), Vec3::new(0, 0, 0).indexify3(Vec3::new(0, 0, 0)));
		assert_eq!(Vec3::new(0, 0, 0).indexify(4), Vec3::new(0, 0, 0).indexify3(Vec3::new_xyz(4)));

		assert_eq!(Vec3::new(1, 0, 0).indexify(4), Vec3::new(1, 0, 0).indexify3(Vec3::new_xyz(4)));
		assert_eq!(Vec3::new(2, 0, 0).indexify(4), Vec3::new(2, 0, 0).indexify3(Vec3::new_xyz(4)));
		assert_eq!(Vec3::new(3, 0, 0).indexify(4), Vec3::new(3, 0, 0).indexify3(Vec3::new_xyz(4)));
		assert_eq!(Vec3::new(4, 0, 0).indexify(4), Vec3::new(4, 0, 0).indexify3(Vec3::new_xyz(4)));

		assert_eq!(Vec3::new(0, 1, 0).indexify(4), Vec3::new(0, 1, 0).indexify3(Vec3::new_xyz(4)));
		assert_eq!(Vec3::new(1, 1, 0).indexify(4), Vec3::new(1, 1, 0).indexify3(Vec3::new_xyz(4)));
		assert_eq!(Vec3::new(2, 1, 0).indexify(4), Vec3::new(2, 1, 0).indexify3(Vec3::new_xyz(4)));
		assert_eq!(Vec3::new(3, 1, 0).indexify(4), Vec3::new(3, 1, 0).indexify3(Vec3::new_xyz(4)));
		assert_eq!(Vec3::new(0, 2, 0).indexify(4), Vec3::new(0, 2, 0).indexify3(Vec3::new_xyz(4)));
		assert_eq!(Vec3::new(0, 3, 0).indexify(4), Vec3::new(0, 3, 0).indexify3(Vec3::new_xyz(4)));
		assert_eq!(Vec3::new(0, 4, 0).indexify(4), Vec3::new(0, 4, 0).indexify3(Vec3::new_xyz(4)));

		assert_eq!(Vec3::new(0, 0, 1).indexify(4), Vec3::new(0, 0, 1).indexify3(Vec3::new_xyz(4)));
		assert_eq!(Vec3::new(0, 0, 2).indexify(4), Vec3::new(0, 0, 2).indexify3(Vec3::new_xyz(4)));
		assert_eq!(Vec3::new(0, 0, 3).indexify(4), Vec3::new(0, 0, 3).indexify3(Vec3::new_xyz(4)));
		assert_eq!(Vec3::new(0, 0, 4).indexify(4), Vec3::new(0, 0, 4).indexify3(Vec3::new_xyz(4)));
	}

	#[test]
	fn vec_len_sqr() {
		assert_eq!(Vec3::new(0, 0, 0).len_sqr(), 0);
		assert_eq!(Vec3::new(0, 1, 0).len_sqr(), 1);
		assert!(approx_eq(Vec3::new(3.0, 4.0, 5.0).len_sqr(), 3.0 * 3.0 + 4.0 * 4.0 + 5.0 * 5.0, BIG_EPSILON));
	}

	#[test]
	fn vec_dot() {
		assert_eq!(Vec3::new(0, 0, 0).dot(Vec3::new(0, 0, 0)), 0);
		assert_eq!(Vec3::new(0, 1, 0).dot(Vec3::new(0, 0, 1)), 0);
		assert_eq!(Vec3::new(0, 1, 0).dot(Vec3::new(0, 1, 0)), 1);
		assert_eq!(Vec3::new(1, 2, 3).dot(Vec3::new(4, 5, 6)), 32);
		let res = Vec3::new(0.5, 0.8, 0.3).dot(Vec3::new(0.2, 0.4, 0.6));
		assert!(approx_eq(res, 0.6, f32::EPSILON));
	}

	#[test]
	fn vec_cross() {
		assert_eq!(Vec3::new(0, 0, 0).cross(Vec3::new(0, 0, 0)), Vec3::new(0, 0, 0));
		assert_eq!(Vec3::new(1, 0, 0).cross(Vec3::new(0, 1, 0)), Vec3::new(0, 0, 1));
		assert_eq!(Vec3::new(1, 2, 3).cross(Vec3::new(4, 5, 6)), Vec3::new(-3, 6, -3));
		let res = Vec3::new(0.5, 0.8, 0.3).cross(Vec3::new(0.2, 0.4, 0.6));
		assert!(approx_eq(res.x, 0.36, f32::EPSILON));
		assert!(approx_eq(res.y, -0.24, f32::EPSILON));
		assert!(approx_eq(res.z, 0.04, f32::EPSILON));
	}
}
