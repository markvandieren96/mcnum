#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Side {
	Front,
	Back,
	Left,
	Right,
	Top,
	Bottom,
}

impl Side {
	pub fn to_index(self) -> usize {
		match self {
			Side::Front => 0,
			Side::Back => 1,
			Side::Left => 2,
			Side::Right => 3,
			Side::Top => 4,
			Side::Bottom => 5,
		}
	}
}
