use super::Vec3;
use serde::{Deserialize, Serialize};
use std::ops::{Add, AddAssign, Div, Mul, Rem, Sub};

pub type Vec2f = glam::Vec2;
pub type Vec2i = Vec2<i32>;
pub type Vec2u = Vec2<u32>;

#[derive(Copy, Clone, Eq, Debug, Default, PartialEq, PartialOrd, Ord, Serialize, Deserialize, Hash)]
pub struct Vec2<T> {
	pub x: T,
	pub y: T,
}

impl<T> From<[T; 2]> for Vec2<T>
where
	T: Copy,
{
	fn from(v: [T; 2]) -> Self {
		Vec2 { x: v[0], y: v[1] }
	}
}

impl<T> Vec2<T> {
	pub fn zero() -> Vec2<T>
	where
		T: std::default::Default,
	{
		Vec2 { x: Default::default(), y: Default::default() }
	}

	pub fn new(x: T, y: T) -> Self {
		Vec2 { x, y }
	}

	pub fn new_xy(xy: T) -> Self
	where
		T: Copy,
	{
		Vec2 { x: xy, y: xy }
	}

	pub fn indexify(&self, dimension: T) -> T
	where
		T: Mul<Output = T> + Add<Output = T> + Copy,
	{
		self.x + self.y * dimension
	}

	pub fn indexify2(&self, dimensions: Self) -> T
	where
		T: Mul<Output = T> + Add<Output = T> + Copy,
	{
		self.x + self.y * dimensions.x
	}

	pub fn swap_components(&self) -> Vec2<T>
	where
		T: Copy,
	{
		Vec2::new(self.y, self.x)
	}

	pub fn len_sqr(&self) -> T
	where
		T: Mul<Output = T> + Add<Output = T> + Copy,
	{
		self.x * self.x + self.y * self.y
	}

	pub fn to_3d(&self) -> Vec3<T>
	where
		T: std::default::Default + Clone,
	{
		Vec3::new(self.x.clone(), self.y.clone(), T::default())
	}
}

impl<T> Vec2<T>
where
	T: num::traits::Float,
{
	pub fn length(&self) -> T
	where
		T: Mul<Output = T> + Add<Output = T> + Copy,
	{
		self.len_sqr().sqrt()
	}

	pub fn normalized(&self) -> Option<Vec2<T>>
	where
		T: Mul<Output = T> + Div<Output = T> + Add<Output = T> + Copy,
	{
		let len = self.len_sqr().sqrt();
		if len.is_zero() {
			None
		} else {
			Some(*self / len)
		}
	}

	pub fn dot(&self, other: Vec2<T>) -> T {
		self.x * other.x + self.y * other.y
	}
}

impl Vec2<f32> {
	pub fn to_vec2i(self) -> Vec2i {
		Vec2::new(self.x as i32, self.y as i32)
	}
}

impl Vec2<i32> {
	pub fn to_vec2f(self) -> Vec2f {
		Vec2f::new(self.x as f32, self.y as f32)
	}

	pub fn left(self) -> Vec2i {
		Vec2::new(self.x - 1, self.y)
	}

	pub fn right(self) -> Vec2i {
		Vec2::new(self.x + 1, self.y)
	}

	pub fn bottom(self) -> Vec2i {
		Vec2::new(self.x, self.y - 1)
	}

	pub fn top(self) -> Vec2i {
		Vec2::new(self.x, self.y + 1)
	}
}

impl From<Vec2i> for Vec2f {
	fn from(v: Vec2i) -> Vec2f {
		Vec2f::new(v.x as f32, v.y as f32)
	}
}

impl From<Vec2f> for Vec2i {
	fn from(v: Vec2f) -> Vec2i {
		Vec2::new(v.x as i32, v.y as i32)
	}
}

impl From<Vec2<f64>> for Vec2i {
	fn from(v: Vec2<f64>) -> Vec2i {
		Vec2::new(v.x as i32, v.y as i32)
	}
}

impl From<Vec2i> for Vec2<f64> {
	fn from(v: Vec2i) -> Vec2<f64> {
		Vec2::new(v.x as f64, v.y as f64)
	}
}

impl From<Vec2<u32>> for Vec2f {
	fn from(v: Vec2<u32>) -> Vec2f {
		Vec2f::new(v.x as f32, v.y as f32)
	}
}

impl From<Vec2<u32>> for Vec2i {
	fn from(v: Vec2<u32>) -> Vec2i {
		Vec2::new(v.x as i32, v.y as i32)
	}
}

impl From<Vec2<f32>> for Vec2<f64> {
	fn from(v: Vec2<f32>) -> Vec2<f64> {
		Vec2::new(v.x.into(), v.y.into())
	}
}

impl From<Vec2<f64>> for Vec2<f32> {
	fn from(v: Vec2<f64>) -> Vec2<f32> {
		Vec2::new(v.x as f32, v.y as f32)
	}
}

impl From<Vec2f> for Vec2<f32> {
	fn from(v: Vec2f) -> Vec2<f32> {
		Vec2::new(v.x, v.y)
	}
}

impl From<Vec2<f32>> for Vec2f {
	fn from(v: Vec2<f32>) -> Vec2f {
		Vec2f::new(v.x, v.y)
	}
}

impl<T> Add<Vec2<T>> for Vec2<T>
where
	T: Add<Output = T> + Copy,
{
	type Output = Vec2<T>;

	fn add(self, other: Self) -> Self {
		Vec2::new(self.x + other.x, self.y + other.y)
	}
}

impl<T> Sub<Vec2<T>> for Vec2<T>
where
	T: Sub<Output = T> + Copy,
{
	type Output = Self;

	fn sub(self, other: Self) -> Self {
		Vec2::new(self.x - other.x, self.y - other.y)
	}
}

impl<T> Rem<T> for Vec2<T>
where
	T: Rem<Output = T> + Copy,
{
	type Output = Self;

	fn rem(self, val: T) -> Self {
		Vec2::new(self.x % val, self.y % val)
	}
}

impl<T> Mul<T> for Vec2<T>
where
	T: Mul<Output = T> + Copy,
{
	type Output = Self;

	fn mul(self, val: T) -> Self {
		Vec2::new(self.x * val, self.y * val)
	}
}

impl<T> Mul<Vec2<T>> for Vec2<T>
where
	T: Mul<Output = T> + Copy,
{
	type Output = Self;

	fn mul(self, val: Vec2<T>) -> Self {
		Vec2::new(self.x * val.x, self.y * val.y)
	}
}

impl<T> Div<T> for Vec2<T>
where
	T: Div<Output = T> + Copy,
{
	type Output = Self;

	fn div(self, val: T) -> Self {
		Vec2::new(self.x / val, self.y / val)
	}
}

impl<T> Div<Vec2<T>> for Vec2<T>
where
	T: Div<Output = T> + Copy,
{
	type Output = Self;

	fn div(self, other: Self) -> Self {
		Vec2::new(self.x / other.x, self.y / other.y)
	}
}

impl<T> AddAssign<Vec2<T>> for Vec2<T>
where
	T: Add<Output = T> + Copy,
{
	fn add_assign(&mut self, other: Vec2<T>) {
		*self = *self + other;
	}
}

impl<T> std::fmt::Display for Vec2<T>
where
	T: std::fmt::Display,
{
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "({}, {})", self.x, self.y)
	}
}

impl<T> std::ops::Index<usize> for Vec2<T>
where
	T: Default + Clone,
{
	type Output = T;

	fn index(&self, index: usize) -> &T {
		match index {
			0 => &self.x,
			1 => &self.y,
			_ => {
				println!("Index is not in range! index={}, size=2", index);
				panic!("Index is not in range!")
			}
		}
	}
}

impl<T> std::ops::IndexMut<usize> for Vec2<T>
where
	T: Default + Clone,
{
	fn index_mut(&mut self, index: usize) -> &mut T {
		match index {
			0 => &mut self.x,
			1 => &mut self.y,
			_ => {
				println!("Index is not in range! index={}, size=2", index);
				panic!("Index is not in range!")
			}
		}
	}
}

#[cfg(test)]
mod test {
	use super::Vec2;
	#[test]
	fn vec_equality() {
		assert_eq!(Vec2::new(4, 3), Vec2::new(4, 3));
		assert_ne!(Vec2::new(4, 3), Vec2::new(4, 0));
		assert_ne!(Vec2::new(4, 3), Vec2::new(3, 4));
	}

	#[test]
	fn vec_new() {
		assert_eq!(Vec2::new(4, 3), Vec2 { x: 4, y: 3 });
	}

	#[test]
	fn vec_new_xyz() {
		assert_eq!(Vec2::new_xy(4), Vec2 { x: 4, y: 4 });
	}

	#[test]
	fn vec_zero() {
		assert_eq!(Vec2::new(0, 0), Vec2::zero());
		assert_eq!(Vec2 { x: 0, y: 0 }, Vec2::zero());
	}

	#[test]
	fn vec_ops() {
		assert_eq!(Vec2::new(4, 3) + Vec2::new(4, 3), Vec2::new(8, 6));
		assert_eq!(Vec2::new(4, 3) - Vec2::new(4, 3), Vec2::new(0, 0));

		assert_eq!(Vec2::new(4, 3) * 2, Vec2::new(8, 6));
		assert_eq!(Vec2::new(8, 6) / 2, Vec2::new(4, 3));

		assert_eq!(Vec2::new(4, 3) % 2, Vec2::new(0, 1));
		assert_eq!(Vec2::new(8, 6) % 2, Vec2::new(0, 0));
		assert_eq!(Vec2::new(9, 4) % 3, Vec2::new(0, 1));
	}

	#[test]
	fn vec_indexify() {
		assert_eq!(Vec2::new(0, 0).indexify(0), 0);
		assert_eq!(Vec2::new(0, 0).indexify(4), 0);

		assert_eq!(Vec2::new(1, 0).indexify(4), 1);
		assert_eq!(Vec2::new(2, 0).indexify(4), 2);
		assert_eq!(Vec2::new(3, 0).indexify(4), 3);
		assert_eq!(Vec2::new(4, 0).indexify(4), 4);

		assert_eq!(Vec2::new(0, 1).indexify(4), 4);
		assert_eq!(Vec2::new(1, 1).indexify(4), 5);
		assert_eq!(Vec2::new(2, 1).indexify(4), 6);
		assert_eq!(Vec2::new(3, 1).indexify(4), 7);
		assert_eq!(Vec2::new(0, 2).indexify(4), 8);
		assert_eq!(Vec2::new(0, 3).indexify(4), 12);
		assert_eq!(Vec2::new(0, 4).indexify(4), 16);
	}
}
