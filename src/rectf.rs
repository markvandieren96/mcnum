use crate::{Recti, Vec2f};
use serde::{Deserialize, Serialize};
use std::ops::{Div, Mul};

#[derive(Default, Copy, Clone, Debug, Serialize, Deserialize, PartialEq)]
pub struct Rectf {
	pub x: f32,
	pub y: f32,
	pub w: f32,
	pub h: f32,
}

impl Rectf {
	pub fn new(x: f32, y: f32, w: f32, h: f32) -> Rectf {
		Rectf { x, y, w, h }
	}
	pub fn new_pos_size(pos: Vec2f, size: Vec2f) -> Rectf {
		Rectf::new(pos.x, pos.y, size.x, size.y)
	}

	pub fn new_min_max(min: Vec2f, max: Vec2f) -> Rectf {
		Rectf::new_pos_size(min, max - min)
	}

	pub fn pos(self) -> Vec2f {
		Vec2f::new(self.x, self.y)
	}

	pub fn size(self) -> Vec2f {
		Vec2f::new(self.w, self.h)
	}

	pub fn with_pos(self, pos: Vec2f) -> Self {
		Rectf::new_pos_size(pos, self.size())
	}

	pub fn with_size(self, size: Vec2f) -> Self {
		Rectf::new_pos_size(self.pos(), size)
	}

	pub fn translate(self, translation: Vec2f) -> Self {
		Rectf::new_pos_size(self.pos() + translation, self.size())
	}

	pub fn scale(self, scale: f32) -> Self {
		Rectf::new_pos_size(self.pos(), self.size() * Vec2f::splat(scale))
	}

	pub fn scale2(self, scale: Vec2f) -> Self {
		Rectf::new_pos_size(self.pos(), self.size() * scale)
	}

	pub fn contains_point(self, point: Vec2f) -> bool {
		point.x >= self.min().x && point.y >= self.min().y && point.x < self.max().x && point.y < self.max().y
	}

	pub fn min(self) -> Vec2f {
		Vec2f::new(self.x, self.y)
	}

	pub fn max(self) -> Vec2f {
		Vec2f::new(self.x + self.w, self.y + self.h)
	}

	pub fn fixit(&mut self) {
		if self.w < 0.0 {
			self.x += self.w;
			self.w = self.w.abs();
		}
		if self.y < 0.0 {
			self.y += self.h;
			self.h = self.h.abs();
		}
	}
}

impl Rectf {
	pub fn new_unit() -> Rectf {
		Rectf::new_pos_size(Vec2f::ZERO, Vec2f::splat(1.0))
	}

	pub fn center(&self) -> Vec2f {
		self.min() + self.size() * 0.5
	}
}

impl From<Recti> for Rectf {
	fn from(rect: Recti) -> Rectf {
		Rectf::new(rect.x as f32, rect.y as f32, rect.w as f32, rect.h as f32)
	}
}

impl Div<Vec2f> for Rectf {
	type Output = Rectf;

	fn div(self, v: Vec2f) -> Rectf {
		Rectf {
			x: self.x / v.x,
			y: self.y / v.y,
			w: self.w / v.x,
			h: self.h / v.y,
		}
	}
}

impl Mul<f32> for Rectf {
	type Output = Rectf;

	fn mul(self, f: f32) -> Rectf {
		Rectf {
			x: self.x * f,
			y: self.y * f,
			w: self.w * f,
			h: self.h * f,
		}
	}
}
