use super::{Quat, Vec3f};
use std::ops::Mul;

type GlamMat4 = glam::f32::Mat4;

#[derive(Debug, Copy, Clone)]
pub struct Mat4(pub GlamMat4);

impl Mat4 {
	pub fn identity() -> Self {
		Self(GlamMat4::IDENTITY)
	}

	#[allow(clippy::too_many_arguments)]
	pub fn new(m11: f32, m12: f32, m13: f32, m14: f32, m21: f32, m22: f32, m23: f32, m24: f32, m31: f32, m32: f32, m33: f32, m34: f32, m41: f32, m42: f32, m43: f32, m44: f32) -> Self {
		Self(GlamMat4::from_cols_array(&[m11, m21, m31, m41, m12, m22, m32, m42, m13, m23, m33, m43, m14, m24, m34, m44]))
	}

	pub fn view_fps(eye_pos: Vec3f, pitch: f32, yaw: f32) -> Self {
		let cos_pitch = pitch.cos();
		let sin_pitch = pitch.sin();
		let cos_yaw = yaw.cos();
		let sin_yaw = yaw.sin();

		let xaxis = Vec3f::new(cos_yaw, 0.0, -sin_yaw);
		let yaxis = Vec3f::new(sin_yaw * sin_pitch, cos_pitch, cos_yaw * sin_pitch);
		let zaxis = Vec3f::new(sin_yaw * cos_pitch, -sin_pitch, cos_pitch * cos_yaw);

		let dot_x = xaxis.dot(eye_pos);
		let dot_y = yaxis.dot(eye_pos);
		let dot_z = zaxis.dot(eye_pos);

		Self::new(xaxis.x, xaxis.y, xaxis.z, -dot_x, yaxis.x, yaxis.y, yaxis.z, -dot_y, zaxis.x, zaxis.y, zaxis.z, -dot_z, 0.0, 0.0, 0.0, 1.0)
	}

	pub fn perspective(aspect: f32, fovy: f32, near: f32, far: f32) -> Self {
		Self(GlamMat4::perspective_rh_gl(fovy, aspect, near, far))
	}

	pub fn ortho(left: f32, right: f32, bottom: f32, top: f32, near: f32, far: f32) -> Self {
		Self(GlamMat4::orthographic_rh_gl(left, right, bottom, top, near, far))
	}

	pub fn translate(mat: &Self, translation: Vec3f) -> Self {
		Self(mat.0 * Self::translation(translation).0)
	}

	pub fn translation(translation: Vec3f) -> Self {
		Self(GlamMat4::from_translation(translation.into()))
	}

	pub fn rotation2(rotation: Quat) -> Self {
		Self(GlamMat4::from_quat(rotation.0.normalize()))
	}

	pub fn scaled(scale: Vec3f) -> Self {
		Self(GlamMat4::from_scale(scale.into()))
	}

	pub fn scale(mat: &Self, scale: Vec3f) -> Self {
		Self(mat.0 * GlamMat4::from_scale(scale.into()))
	}

	pub fn as_pointer(&self) -> *const f32 {
		&self.0.as_ref()[0]
	}
}

impl Mul<Mat4> for Mat4 {
	type Output = Mat4;
	fn mul(self, other: Mat4) -> Mat4 {
		Mat4(self.0 * other.0)
	}
}

impl Mul<Vec3f> for Mat4 {
	type Output = Vec3f;
	fn mul(self, v: Vec3f) -> Vec3f {
		self.0.transform_point3(v.into()).into()
	}
}

impl Default for Mat4 {
	fn default() -> Mat4 {
		Mat4::identity()
	}
}

impl std::ops::Index<usize> for Mat4 {
	type Output = f32;

	fn index(&self, index: usize) -> &Self::Output {
		&self.0.as_ref()[index]
	}
}

impl std::ops::IndexMut<usize> for Mat4 {
	fn index_mut(&mut self, index: usize) -> &mut Self::Output {
		&mut self.0.as_mut()[index]
	}
}
