use crate::Vec2f;
use serde::{Deserialize, Serialize};

#[derive(Default, Clone, PartialEq, Serialize, Deserialize)]
pub struct Curve {
	pub points: Vec<Vec2f>,
}

stid::stid!(Curve);

impl Curve {
	pub fn add_point(&mut self, point: Vec2f) {
		for i in 0..self.points.len() {
			if point.x < self.points[i].x {
				self.points.insert(i, point);
				return;
			}
		}
		self.points.push(point);
	}

	pub fn set(&mut self, x: f32, y: f32) {
		self.add_point(Vec2f::new(x, y));
	}

	pub fn get(&self, x: f32) -> Result<f32, String> {
		if self.points.is_empty() {
			Err("Could not interpolate point on curve, because curve has 0 points!".to_owned())
		} else if self.points.len() == 1 || x <= self.points[0].x {
			Ok(self.points[0].y)
		} else if x >= self.points[self.points.len() - 1].x {
			Ok(self.points[self.points.len() - 1].y)
		} else {
			let mut right_index = 0;
			for i in 0..self.points.len() {
				if self.points[i].x > x {
					right_index = i;
					break;
				}
			}

			let left_point = self.points[right_index - 1];
			let right_point = self.points[right_index];
			let range = right_point - left_point;
			Ok(left_point.y + normalize(x, left_point.x, range.x) * range.y)
		}
	}
}

fn normalize(f: f32, min: f32, range: f32) -> f32 {
	(f - min) / range
}
