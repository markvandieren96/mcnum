use super::*;
use serde::{Deserialize, Serialize};
use std::convert::TryFrom;
use std::{fmt, fmt::Display, ops::*};

stid::stid!(Vec3f);

#[repr(C)]
#[derive(Serialize, Deserialize, Copy, Clone, PartialEq, PartialOrd, Debug, Default)]
pub struct Vec3f {
	pub x: f32,
	pub y: f32,
	pub z: f32,
}

impl From<[f32; 3]> for Vec3f {
	fn from(v: [f32; 3]) -> Self {
		Vec3f { x: v[0], y: v[1], z: v[2] }
	}
}

impl From<(f32, f32, f32)> for Vec3f {
	fn from((x, y, z): (f32, f32, f32)) -> Self {
		Vec3f { x, y, z }
	}
}

impl Vec3f {
	pub const fn new(x: f32, y: f32, z: f32) -> Vec3f {
		Vec3f { x, y, z }
	}

	pub fn splat(xyz: f32) -> Vec3f {
		Vec3f { x: xyz, y: xyz, z: xyz }
	}

	pub fn length_squared(&self) -> f32 {
		self.x * self.x + self.y * self.y + self.z * self.z
	}

	pub fn dot(&self, other: Self) -> f32 {
		self.x * other.x + self.y * other.y + self.z * other.z
	}

	pub fn cross(&self, other: Self) -> Self {
		Self::new(self.y * other.z - self.z * other.y, self.z * other.x - self.x * other.z, self.x * other.y - self.y * other.x)
	}

	pub fn xz(&self) -> Vec2f {
		Vec2f::new(self.x, self.z)
	}

	pub fn xy(&self) -> Vec2f {
		Vec2f::new(self.x, self.y)
	}

	pub const ZERO: Vec3f = Vec3f::new(0.0, 0.0, 0.0);
	pub const LEFT: Vec3f = Vec3f::new(-1.0, 0.0, 0.0);
	pub const RIGHT: Vec3f = Vec3f::new(1.0, 0.0, 0.0);
	pub const BOTTOM: Vec3f = Vec3f::new(0.0, -1.0, 0.0);
	pub const TOP: Vec3f = Vec3f::new(0.0, 1.0, 0.0);
	pub const BACK: Vec3f = Vec3f::new(0.0, 0.0, -1.0);
	pub const FRONT: Vec3f = Vec3f::new(0.0, 0.0, 1.0);

	pub fn pitch(&self) -> f32 {
		self.y.asin()
	}

	pub fn yaw(&self) -> f32 {
		self.x.atan2(self.z)
	}

	pub fn from_pitch_yaw(pitch: f32, yaw: f32) -> Vec3f {
		Vec3f::new(yaw.sin() * pitch.cos(), pitch.sin(), yaw.cos() * pitch.cos())
	}

	pub fn length(&self) -> f32 {
		self.length_squared().sqrt()
	}

	pub fn normalized(self) -> Vec3f {
		let len = self.length_squared().sqrt();
		if len == 0.0 {
			panic!("Attempted to normalize a Vec3f of length 0")
		} else {
			self / len
		}
	}

	pub fn normalized_or_zero(self) -> Vec3f {
		let len = self.length_squared().sqrt();
		if len == 0.0 {
			Self::ZERO
		} else {
			self / len
		}
	}

	pub fn try_normalized(self) -> Option<Vec3f> {
		let len = self.length_squared().sqrt();
		if len == 0.0 {
			None
		} else {
			Some(self / len)
		}
	}

	pub fn left(&self) -> Vec3f {
		Vec3f::new(self.x - 1.0, self.y, self.z)
	}

	pub fn right(&self) -> Vec3f {
		Vec3f::new(self.x + 1.0, self.y, self.z)
	}

	pub fn bottom(&self) -> Vec3f {
		Vec3f::new(self.x, self.y - 1.0, self.z)
	}

	pub fn top(&self) -> Vec3f {
		Vec3f::new(self.x, self.y + 1.0, self.z)
	}

	pub fn back(&self) -> Vec3f {
		Vec3f::new(self.x, self.y, self.z - 1.0)
	}

	pub fn front(&self) -> Vec3f {
		Vec3f::new(self.x, self.y, self.z + 1.0)
	}

	pub fn as_index(&self) -> (usize, usize, usize) {
		(self.x as usize, self.y as usize, self.z as usize)
	}

	pub fn abs(&self) -> Vec3f {
		Vec3f::new(self.x.abs(), self.y.abs(), self.z.abs())
	}

	pub fn min(&self, f: f32) -> Vec3f {
		Vec3f::new(self.x.min(f), self.y.min(f), self.z.min(f))
	}

	pub fn max(&self, f: f32) -> Vec3f {
		Vec3f::new(self.x.max(f), self.y.max(f), self.z.max(f))
	}

	pub fn min_v(&self, v: Vec3f) -> Vec3f {
		Vec3f::new(self.x.min(v.x), self.y.min(v.y), self.z.min(v.z))
	}

	pub fn max_v(&self, v: Vec3f) -> Vec3f {
		Vec3f::new(self.x.max(v.x), self.y.max(v.y), self.z.max(v.z))
	}

	pub fn min_component(&self) -> f32 {
		self.x.min(self.y.min(self.z))
	}

	pub fn max_component(&self) -> f32 {
		self.x.max(self.y.max(self.z))
	}

	pub fn to_glam(self) -> glam::Vec3 {
		glam::Vec3::new(self.x, self.y, self.z)
	}

	pub fn to_vec3i(self) -> Vec3i {
		Vec3i::new(self.x as i32, self.y as i32, self.z as i32)
	}
}

impl From<Vec3f> for Vec3i {
	fn from(v: Vec3f) -> Self {
		Vec3i::new(v.x as i32, v.y as i32, v.z as i32)
	}
}

impl From<Vec3i> for Vec3f {
	fn from(v: Vec3i) -> Self {
		Vec3f::new(v.x as f32, v.y as f32, v.z as f32)
	}
}

impl From<Vec3u> for Vec3f {
	fn from(v: Vec3u) -> Self {
		Vec3f::new(v.x as f32, v.y as f32, v.z as f32)
	}
}

impl From<Vec3f> for Vec3u {
	fn from(v: Vec3f) -> Self {
		Vec3u::new(v.x as u32, v.y as u32, v.z as u32)
	}
}

// Expects the format "(x, y, z)"
impl TryFrom<&str> for Vec3f {
	type Error = ();

	fn try_from(s: &str) -> Result<Self, ()> {
		let mut iter = s[1..s.len() - 1].split(',').map(|s| s.trim()).map(|s| s.parse::<f32>().ok());
		Ok(Vec3f::new(iter.next().flatten().ok_or(())?, iter.next().flatten().ok_or(())?, iter.next().flatten().ok_or(())?))
	}
}

impl From<glam::f32::Vec3> for Vec3f {
	fn from(v: glam::f32::Vec3) -> Self {
		Vec3f::new(v.x, v.y, v.z)
	}
}

impl From<Vec3f> for glam::f32::Vec3 {
	fn from(v: Vec3f) -> Self {
		glam::f32::Vec3::new(v.x, v.y, v.z)
	}
}

impl Mul<GlamQuat> for Vec3f {
	type Output = Vec3f;
	fn mul(self, q: GlamQuat) -> Vec3f {
		q.mul_vec3(self.into()).into()
	}
}

impl Mul<Vec3f> for GlamQuat {
	type Output = Vec3f;
	fn mul(self, v: Vec3f) -> Vec3f {
		self.mul_vec3(v.into()).into()
	}
}

impl Add<Vec3f> for Vec3f {
	type Output = Vec3f;

	fn add(self, other: Vec3f) -> Vec3f {
		Vec3f::new(self.x + other.x, self.y + other.y, self.z + other.z)
	}
}

impl AddAssign<Vec3f> for Vec3f {
	fn add_assign(&mut self, other: Vec3f) {
		*self = *self + other;
	}
}

impl Sub<Vec3f> for Vec3f {
	type Output = Vec3f;

	fn sub(self, other: Vec3f) -> Vec3f {
		Vec3f::new(self.x - other.x, self.y - other.y, self.z - other.z)
	}
}

impl SubAssign<Vec3f> for Vec3f {
	fn sub_assign(&mut self, other: Vec3f) {
		*self = *self - other;
	}
}

impl Rem<f32> for Vec3f {
	type Output = Vec3f;

	fn rem(self, val: f32) -> Vec3f {
		Vec3f::new(self.x % val, self.y % val, self.z % val)
	}
}

impl Mul<f32> for Vec3f {
	type Output = Vec3f;

	fn mul(self, val: f32) -> Vec3f {
		Vec3f::new(self.x * val, self.y * val, self.z * val)
	}
}

impl Mul<Vec3f> for Vec3f {
	type Output = Vec3f;

	fn mul(self, val: Vec3f) -> Vec3f {
		Vec3f::new(self.x * val.x, self.y * val.y, self.z * val.z)
	}
}

impl MulAssign<Vec3f> for Vec3f {
	fn mul_assign(&mut self, other: Vec3f) {
		*self = *self * other;
	}
}

impl Div<f32> for Vec3f {
	type Output = Vec3f;

	fn div(self, val: f32) -> Vec3f {
		Vec3f::new(self.x / val, self.y / val, self.z / val)
	}
}

impl Div<Vec3f> for Vec3f {
	type Output = Vec3f;

	fn div(self, other: Vec3f) -> Vec3f {
		Vec3f::new(self.x / other.x, self.y / other.y, self.z / other.z)
	}
}

impl Neg for Vec3f {
	type Output = Vec3f;

	fn neg(self) -> Vec3f {
		Vec3f::new(-self.x, -self.y, -self.z)
	}
}

impl Display for Vec3f {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "({}, {}, {})", self.x, self.y, self.z)
	}
}

impl Index<usize> for Vec3f {
	type Output = f32;

	fn index(&self, index: usize) -> &f32 {
		match index {
			0 => &self.x,
			1 => &self.y,
			2 => &self.z,
			_ => {
				println!("Index is not in range! index={}, size=3", index);
				panic!("Index is not in range!")
			}
		}
	}
}

impl IndexMut<usize> for Vec3f {
	fn index_mut(&mut self, index: usize) -> &mut f32 {
		match index {
			0 => &mut self.x,
			1 => &mut self.y,
			2 => &mut self.z,
			_ => {
				println!("Index is not in range! index={}, size=3", index);
				panic!("Index is not in range!")
			}
		}
	}
}

#[cfg(test)]
mod test {
	use super::Vec3f;

	#[test]
	fn vec_equality() {
		assert_eq!(Vec3f::new(4.0, 3.0, 2.0), Vec3f::new(4.0, 3.0, 2.0));
		assert_ne!(Vec3f::new(4.0, 3.0, 2.0), Vec3f::new(4.0, 3.0, 0.0));

		assert_eq!(Vec3f::new(4.0, 3.0, 2.0), Vec3f::new(4.0, 3.0, 2.0));
		assert_ne!(Vec3f::new(4.0, 3.0, 2.0), Vec3f::new(2.0, 3.0, 4.0));
	}

	#[test]
	fn vec_new() {
		assert_eq!(Vec3f::new(4.0, 3.0, 2.0), Vec3f { x: 4.0, y: 3.0, z: 2.0 });
	}

	#[test]
	fn vec_new_xyz() {
		assert_eq!(Vec3f::splat(4.0), Vec3f { x: 4.0, y: 4.0, z: 4.0 });
	}

	#[test]
	fn vec_zero() {
		assert_eq!(Vec3f::new(0.0, 0.0, 0.0), Vec3f::ZERO);
		assert_eq!(Vec3f { x: 0.0, y: 0.0, z: 0.0 }, Vec3f::ZERO);
	}

	#[test]
	fn vec_ops() {
		assert_eq!(Vec3f::new(4.0, 3.0, 2.0) + Vec3f::new(4.0, 3.0, 2.0), Vec3f::new(8.0, 6.0, 4.0));
		assert_eq!(Vec3f::new(4.0, 3.0, 2.0) - Vec3f::new(4.0, 3.0, 2.0), Vec3f::new(0.0, 0.0, 0.0));

		assert_eq!(Vec3f::new(4.0, 3.0, 2.0) * 2.0, Vec3f::new(8.0, 6.0, 4.0));
		assert_eq!(Vec3f::new(8.0, 6.0, 4.0) / 2.0, Vec3f::new(4.0, 3.0, 2.0));

		assert_eq!(Vec3f::new(4.0, 3.0, 2.0) % 2.0, Vec3f::new(0.0, 1.0, 0.0));
		assert_eq!(Vec3f::new(8.0, 6.0, 4.0) % 2.0, Vec3f::new(0.0, 0.0, 0.0));
		assert_eq!(Vec3f::new(9.0, 4.0, 7.0) % 3.0, Vec3f::new(0.0, 1.0, 1.0));
	}

	#[test]
	fn vec_len_sqr() {
		approx::assert_abs_diff_eq!(Vec3f::new(0.0, 0.0, 0.0).length_squared(), 0.0);
		approx::assert_abs_diff_eq!(Vec3f::new(0.0, 1.0, 0.0).length_squared(), 1.0);
		approx::assert_abs_diff_eq!(Vec3f::new(3.0, 4.0, 5.0).length_squared(), 3.0 * 3.0 + 4.0 * 4.0 + 5.0 * 5.0);
	}

	#[test]
	fn vec_dot() {
		approx::assert_abs_diff_eq!(Vec3f::new(0.0, 0.0, 0.0).dot(Vec3f::new(0.0, 0.0, 0.0)), 0.0);
		approx::assert_abs_diff_eq!(Vec3f::new(0.0, 1.0, 0.0).dot(Vec3f::new(0.0, 0.0, 1.0)), 0.0);
		approx::assert_abs_diff_eq!(Vec3f::new(0.0, 1.0, 0.0).dot(Vec3f::new(0.0, 1.0, 0.0)), 1.0);
		approx::assert_abs_diff_eq!(Vec3f::new(1.0, 2.0, 3.0).dot(Vec3f::new(4.0, 5.0, 6.0)), 32.0);
		approx::assert_abs_diff_eq!(Vec3f::new(0.5, 0.8, 0.3).dot(Vec3f::new(0.2, 0.4, 0.6)), 0.6);
	}

	#[test]
	fn vec_cross() {
		assert_vec_approx_eq(Vec3f::new(0.0, 0.0, 0.0).cross(Vec3f::new(0.0, 0.0, 0.0)), Vec3f::new(0.0, 0.0, 0.0));
		assert_vec_approx_eq(Vec3f::new(1.0, 0.0, 0.0).cross(Vec3f::new(0.0, 1.0, 0.0)), Vec3f::new(0.0, 0.0, 1.0));
		assert_vec_approx_eq(Vec3f::new(1.0, 2.0, 3.0).cross(Vec3f::new(4.0, 5.0, 6.0)), Vec3f::new(-3.0, 6.0, -3.0));
		assert_vec_approx_eq(Vec3f::new(0.5, 0.8, 0.3).cross(Vec3f::new(0.2, 0.4, 0.6)), Vec3f::new(0.36, -0.24, 0.04));
	}

	fn assert_vec_approx_eq(a: Vec3f, b: Vec3f) {
		approx::assert_abs_diff_eq!(a.x, b.x);
		approx::assert_abs_diff_eq!(a.y, b.y);
		approx::assert_abs_diff_eq!(a.z, b.z);
	}
}
