use crate::Vec2;

pub struct Triangle {
	pub v0: Vec2<f64>,
	pub v1: Vec2<f64>,
	pub v2: Vec2<f64>,
}

impl Triangle {
	pub fn new(v0: Vec2<f64>, v1: Vec2<f64>, v2: Vec2<f64>) -> Triangle {
		Triangle { v0, v1, v2 }
	}

	// https://gamedev.stackexchange.com/questions/23743/whats-the-most-efficient-way-to-find-barycentric-coordinates
	pub fn uv(&self, p: Vec2<f64>) -> Vec2<f64> {
		let a = self.v0;
		let b = self.v1;
		let c = self.v2;

		let v0 = c - a;
		let v1 = b - a;
		let v2 = p - a;

		let dot00 = v0.dot(v0);
		let dot01 = v0.dot(v1);
		let dot02 = v0.dot(v2);
		let dot11 = v1.dot(v1);
		let dot12 = v1.dot(v2);

		let inv_denom = 1.0 / (dot00 * dot11 - dot01.powf(2.0));

		Vec2::new((dot11 * dot02 - dot01 * dot12) * inv_denom, (dot00 * dot12 - dot01 * dot02) * inv_denom)
	}

	pub fn is_inside(uv: Vec2<f64>) -> bool {
		(uv.x >= 0.0) && (uv.y >= 0.0) && (uv.x + uv.y <= 1.0)
	}
}
