use super::{Vec2, Vec3};
use fmt::Display;
use serde::{Deserialize, Serialize};
use std::fmt;
use std::ops::{Add, Div, Index, IndexMut, Mul, Rem, Sub};

pub type Vec4f = Vec4<f32>;

#[derive(Copy, Clone, Eq, PartialEq, Debug, Hash, Default, Serialize, Deserialize)]
pub struct Vec4<T> {
	pub x: T,
	pub y: T,
	pub z: T,
	pub w: T,
}

impl<T> From<[T; 4]> for Vec4<T>
where
	T: Copy,
{
	fn from(v: [T; 4]) -> Self {
		Vec4 { x: v[0], y: v[1], z: v[2], w: v[3] }
	}
}

impl<T> Vec4<T> {
	pub fn zero() -> Vec4<T>
	where
		T: Default,
	{
		Vec4 {
			x: Default::default(),
			y: Default::default(),
			z: Default::default(),
			w: Default::default(),
		}
	}

	pub fn new(x: T, y: T, z: T, w: T) -> Vec4<T> {
		Vec4 { x, y, z, w }
	}

	pub fn new_xyzw(xyzw: T) -> Vec4<T>
	where
		T: Copy,
	{
		Vec4 { x: xyzw, y: xyzw, z: xyzw, w: xyzw }
	}

	pub fn to_vec3(&self) -> Vec3<T>
	where
		T: Clone,
	{
		Vec3::new(self.x.clone(), self.y.clone(), self.z.clone())
	}

	pub fn xy(&self) -> Vec2<T>
	where
		T: Clone,
	{
		Vec2::new(self.x.clone(), self.y.clone())
	}

	pub fn xyz(&self) -> Vec3<T>
	where
		T: Clone,
	{
		Vec3::new(self.x.clone(), self.y.clone(), self.z.clone())
	}
}

impl<T> Add<Vec4<T>> for Vec4<T>
where
	T: Add<Output = T> + Copy,
{
	type Output = Vec4<T>;

	fn add(self, other: Vec4<T>) -> Vec4<T> {
		Vec4::new(self.x + other.x, self.y + other.y, self.z + other.z, self.w + other.w)
	}
}

impl<T> Sub<Vec4<T>> for Vec4<T>
where
	T: Sub<Output = T> + Copy,
{
	type Output = Vec4<T>;

	fn sub(self, other: Vec4<T>) -> Vec4<T> {
		Vec4::new(self.x - other.x, self.y - other.y, self.z - other.z, self.w - other.w)
	}
}

impl<T> Rem<T> for Vec4<T>
where
	T: Rem<Output = T> + Copy,
{
	type Output = Vec4<T>;

	fn rem(self, val: T) -> Vec4<T> {
		Vec4::new(self.x % val, self.y % val, self.z % val, self.w % val)
	}
}

impl<T> Mul<T> for Vec4<T>
where
	T: Mul<Output = T> + Copy,
{
	type Output = Vec4<T>;

	fn mul(self, val: T) -> Vec4<T> {
		Vec4::new(self.x * val, self.y * val, self.z * val, self.w * val)
	}
}

impl<T> Div<T> for Vec4<T>
where
	T: Div<Output = T> + Copy,
{
	type Output = Vec4<T>;

	fn div(self, val: T) -> Vec4<T> {
		Vec4::new(self.x / val, self.y / val, self.z / val, self.w / val)
	}
}

impl<T> Div<Vec4<T>> for Vec4<T>
where
	T: Div<Output = T> + Copy,
{
	type Output = Vec4<T>;

	fn div(self, other: Vec4<T>) -> Vec4<T> {
		Vec4::new(self.x / other.x, self.y / other.y, self.z / other.z, self.w / other.w)
	}
}

impl<T> Display for Vec4<T>
where
	T: Display,
{
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "({}, {}, {}, {})", self.x, self.y, self.z, self.w)
	}
}

impl<T> Index<usize> for Vec4<T>
where
	T: Default + Clone,
{
	type Output = T;

	fn index(&self, index: usize) -> &T {
		match index {
			0 => &self.x,
			1 => &self.y,
			2 => &self.z,
			3 => &self.w,
			_ => {
				println!("Index is not in range! index={}, size=4", index);
				panic!("Index is not in range!")
			}
		}
	}
}

impl<T> IndexMut<usize> for Vec4<T>
where
	T: Default + Clone,
{
	fn index_mut(&mut self, index: usize) -> &mut T {
		match index {
			0 => &mut self.x,
			1 => &mut self.y,
			2 => &mut self.z,
			3 => &mut self.w,
			_ => {
				println!("Index is not in range! index={}, size=4", index);
				panic!("Index is not in range!")
			}
		}
	}
}

#[cfg(test)]
mod test {
	use super::Vec4;
	#[test]
	fn vec_equality() {
		assert_eq!(Vec4::new(4, 4, 2, 2), Vec4::new(4, 4, 2, 2));
		assert_ne!(Vec4::new(4, 4, 2, 2), Vec4::new(4, 4, 2, 0));
	}

	#[test]
	fn vec_new() {
		assert_eq!(Vec4::new(4, 4, 2, 2), Vec4 { x: 4, y: 4, z: 2, w: 2 });
	}

	#[test]
	fn vec_new_xyzw() {
		assert_eq!(Vec4::new_xyzw(4), Vec4 { x: 4, y: 4, z: 4, w: 4 });
	}

	#[test]
	fn vec_zero() {
		assert_eq!(Vec4::new(0, 0, 0, 0), Vec4::zero());
		assert_eq!(Vec4 { x: 0, y: 0, z: 0, w: 0 }, Vec4::zero());
	}

	#[test]
	fn vec_ops() {
		assert_eq!(Vec4::new(4, 3, 2, 2) + Vec4::new(4, 3, 2, 2), Vec4::new(8, 6, 4, 4));
		assert_eq!(Vec4::new(4, 3, 2, 2) - Vec4::new(4, 3, 2, 2), Vec4::new(0, 0, 0, 0));

		assert_eq!(Vec4::new(4, 3, 2, 2) * 2, Vec4::new(8, 6, 4, 4));
		assert_eq!(Vec4::new(8, 6, 4, 4) / 2, Vec4::new(4, 3, 2, 2));

		assert_eq!(Vec4::new(4, 3, 2, 2) % 2, Vec4::new(0, 1, 0, 0));
		assert_eq!(Vec4::new(8, 6, 4, 4) % 2, Vec4::new(0, 0, 0, 0));
		assert_eq!(Vec4::new(9, 3, 7, 7) % 4, Vec4::new(1, 3, 3, 3));
	}
}
